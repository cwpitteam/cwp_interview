# CWP Interview Assignment

This assignment is meant to be challenging. The quant team does not frequently hire talent, and every person that joins the team has significant responsibilities and expectations. As much as this assignment is about assessing your ability, it is also a chance for you see if you are excited about the problems we face.

## Getting Started

First clone this repository to your computer.

At CWP, we work primarily in Python and use Anacondas to manage our packages and ensure our research is reproducible. If you don't already have it, download a conda distribution from:

https://conda.io/docs/user-guide/install/index.html.

Create a virtual environment by running:

```conda create -n CWP-assignment pandas numpy jupyter matplotlib```

```source activate CWP-assignment
```


## Answering the questions

The assignment consists of two questions that will ask you to solve an optimization problem.

You can work on the assignments in an interactive notebook by running:

```jupyter-notebook```

Assignment_Q1.ipynb will ask you to implement an existing optimization algorithm from an academic paper. A successful candidate should quickly grasp the applied aspects and be able to code the implementation given the details and pseudo code contained in the paper.

Assignment_Q2.ipynb contains an open-ended "machine learning" style optimization where you are expected to learn a function that performs well on out-of-sample data.

You are free to use any opensource code libraries to aid you in your implementation. Keep in mind that we are assessing your depth of understanding of optimization and your ability to think out of the box.

## Submitting the assignment

Create a conda environment file with your current working environment so we can re-run your code.

```conda env export > requirements.env```

Zip the folder and send it back to us.

## Documentation

Before starting the Question 2, we strongly encourage the candidates to take the time to go through 
the following documentation from the NYISO market (the New York power market). They will present what a locational marginal price, a DA price and a 
RT price are, and thus what a virtual contract is. Those will complement the oral presentation made at the First Interview.
Ideally The candidate should go through them one after the other one:
1) locational marginal price https://www.nyiso.com/documents/20142/3037451/3-LMBP.pdf/f7682e03-e921-eaab-09bf-690524b5ade6 (slides 1-44)
2) DA and RT market https://www.nyiso.com/online-learning (Energy Market Place 1-5 )
3) Virtual trading: https://www.nyiso.com/online-learning (Virtual Trading section 1-7)
